const mongoose = require("mongoose")

const productSchema = new mongoose.Schema({
    name: String,
    image: String,
    descripsion: String
})

const Product = mongoose.model("Product", productSchema, "products")
module.exports = Product