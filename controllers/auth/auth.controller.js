const db = require('../../lowdb/db')
const md5 = require('md5')
module.exports.login = (req, res) => {
    res.render('./auth/login')
}

module.exports.postLogin = (req, res) => {
    const username = req.body.name
    const pass = req.body.password
    const user = db.get('users').find({ name: username }).value()
    if (!user) {
        res.render('./auth/login', {
            errors: [
                'Username not found'
            ]
        })
        return
    }
    if (user.password !== md5(pass)) {
        res.render('./auth/login', {
            errors: [
                'Wrong password'
            ]
        })
        return
    }
    res.cookie('userId', user.id, { signed: true })
    res.redirect('/users')
}