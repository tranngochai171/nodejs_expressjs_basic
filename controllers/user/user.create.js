
const db = require('../../lowdb/db')
const shortid = require('shortid')
const md5 = require('md5')
module.exports.createUserPostController = (req, res) => {
    req.body.id = shortid.generate()
    req.body.password = md5(req.body.password)
    req.body.avatar = req.file.path.split('/').slice(1).join('/')
    db.get('users').push(req.body).write()
    res.redirect('/users')
}
module.exports.createUserController = (req, res) => {
    res.render('./user/create', {
        csrfToken: req.csrfToken()
    })
}

