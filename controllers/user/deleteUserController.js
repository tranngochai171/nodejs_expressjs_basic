const db = require('../../lowdb/db')
const deleteUserController = (req, res) => {
    if(req.params.id) {
        db.get('users').remove({id: req.params.id}).write()
    }
    res.redirect('/users')
}

module.exports = deleteUserController