const db = require('../../lowdb/db')
module.exports.editUserController = (req, res) => {
    if (req.params.id) {
        const user = db.get('users').find({ id: req.params.id }).value()
        if (user) {
            res.render('./user/edit', {
                user: user,
                active: 'users'
            })
        }
    }
    res.redirect('/users')
}

module.exports.editUserPutController = (req, res) => {
    if (req.body.name && req.body.old_pass && req.body.new_pass && req.body.id) {
        const user = db.get('users').find({ id: req.body.id }).value()
        if (user && req.body.old_pass === user.password) {
            user.password = req.body.new_pass
            db.get('users').find({ id: req.body.id }).assign(user).write()
        }
    }
    res.redirect('/users')
}