const db = require('../../lowdb/db')

const userListController = (req, res) => {
    let users = db.get('users').value()
    res.render('./user/index', {
        users: users
    })
}

module.exports = userListController