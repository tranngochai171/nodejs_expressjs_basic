// const db = require('../../lowdb/db')
const PRODUCT_CONSTANT = require('../../assets/js/productConstant.json')
const Product = require('../../models/product.model')
const getProductController = async (req, res) => {
    // let listProducts = db.get('products').value()
    const page = req.query.page ? parseInt(req.query.page, 10) : 1
    const begin = (page - 1) * PRODUCT_CONSTANT.ITEMS_PER_PAGE
    const end = page * PRODUCT_CONSTANT.ITEMS_PER_PAGE
    const count = await Product.find().count()
    const totalPage = Math.round(count / PRODUCT_CONSTANT.ITEMS_PER_PAGE)
    let pagination = {
        currentPage: page,
        prevPage: page - 1,
        nextPage: page + 1,
        showFirstPage: true,
        showLastPage: true,
        totalPage: totalPage
    }
    if (page == 1) {
        delete pagination.prevPage
    } else if (page == totalPage) {
        delete pagination.nextPage
    }
    if (page <= 2) {
        pagination.showFirstPage = false
    } else if (page >= totalPage - 1) {
        pagination.showLastPage = false
    }
    let products = await Product.find().skip(begin).limit(PRODUCT_CONSTANT.ITEMS_PER_PAGE)
    res.render('./product/index', {
        products,
        pagination
    })
}

module.exports = getProductController