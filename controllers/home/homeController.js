const homeController = (req, res) => {
    res.render('./home/index', {
        active: 'home'
    })
}

module.exports = homeController