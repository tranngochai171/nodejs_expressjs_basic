const db = require('../../lowdb/db')
const { value } = require('../../lowdb/db')
const { write } = require('lowdb/adapters/FileSync')
module.exports.addToCartController = (req, res) => {
  const productId = req.params.productId
  const sessionId = req.signedCookies.sessionId
  if(!sessionId) {
    res.redirect('/products')
    return
  }
  const count = db.get('session')
                  .find({id: sessionId})
                  .get(`cart.${productId}`, 0)
                  .value()
  db.get('session')
    .find({id: sessionId})
    .set(`cart.${productId}`, count + 1)
    .write()
  res.redirect('/products')
}