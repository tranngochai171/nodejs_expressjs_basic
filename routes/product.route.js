const express = require('express')
const router = express.Router()

router.get('/', require('../controllers/product/productController'))

module.exports = router