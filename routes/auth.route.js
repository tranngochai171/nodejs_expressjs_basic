const express = require('express')
const route = express.Router()

const controller = require('../controllers/auth/auth.controller')

route.get('/', controller.login)
route.post('/', controller.postLogin)

module.exports = route