const express = require('express')
const router = express.Router()
const cartController = require('../controllers/cart/cartController')

router.get('/product/:productId', cartController.addToCartController)

module.exports = router