const express = require('express')
const route = express.Router()

var multer = require('multer')
var upload = multer({ dest: './assets/uploads' })

const userCreateController = require('../controllers/user/user.create')
const userEditController = require('../controllers/user/user.edit')
// USER LIST PAGE
route.get('/', require('../controllers/user/userListController'))

// CREATE USER PAGE
route.get('/create', userCreateController.createUserController)
route.post('/create', upload.single('avatar'),
    userCreateController.createUserPostController
)

// DELETE USER
route.get('/delete/:id', require('../controllers/user/deleteUserController'))

// EDIT USER
route.get('/edit/:id', userEditController.editUserController)
route.post('/edit', userEditController.editUserPutController)

module.exports = route