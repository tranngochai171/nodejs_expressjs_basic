const shortid = require('shortid')
const db = require('../lowdb/db')
module.exports = (req, res, next) => {
  if (!req.signedCookies.sessionId) {
    const sessionId = shortid.generate()
    res.cookie('sessionId', sessionId, {
      signed: true
    })
    db.get('session').push({
      id: sessionId
    }).write()
  }
  const carts = db.get('session').find({id: req.signedCookies.sessionId}).get('cart').value()
  let total = 0
  for(let item in carts) {
    total += carts[item]
  }
  res.locals.totalCart = total
  res.locals.csrfToken = req.csrfToken()
  next()
}