const db = require('../lowdb/db')
const CONSTANTS = require('../assets/js/constants.json')
module.exports.auth = (req, res, next) => {
    if (!req.signedCookies.userId) {
        res.redirect('/login')
    }
    const user = db.get('users').find({ id: req.signedCookies.userId }).value()
    if (!user) {
        res.redirect('/login')
    }
    res.locals.active = "home"
    for(let item in CONSTANTS.PAGES){
        let re = new RegExp(item, "g")
        if(re.test(req.originalUrl)){
            res.locals.active = CONSTANTS.PAGES[item]
        }
    }
    res.locals.user = user
    next()
}