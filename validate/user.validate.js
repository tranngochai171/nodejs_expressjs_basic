const validateCreateNewUser = (req, res, next) => {
    let errors = []
    errors = checkBlankInput(errors, 'Name', req.body.name)
    errors = checkBlankInput(errors, 'Password', req.body.password)
    if(errors.length > 0) {
        res.render('./user/create', {
            active: 'users',
            user: req.body,
            errors: errors
        })
        return
    }
    next()
}

const checkBlankInput = (errors, name, field) => {
    if(!field) errors.push(`(*) ${name} required`)
    return errors
}

module.exports =  validateCreateNewUser
