require('dotenv').config()
const express = require('express')
const app = express()
const cookieParser = require('cookie-parser')
const csrf = require('csurf')
const PORT = 3002
const authMiddleware = require('./middlewares/auth.middleware')
const sessionMiddleware = require('./middlewares/session.middleware')
const mongoose = require('mongoose')
mongoose.connect(process.env.MONGO_URL)

app.set('view engine', 'pug')
app.set('views', './views')

app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(express.static('./assets'))
app.use(cookieParser(process.env.SECRET))
app.use(csrf({ cookie: true }))

app.use(sessionMiddleware)

app.use('/login', require('./routes/auth.route'))
app.get('/', authMiddleware.auth, require('./controllers/home/homeController'))
app.get('/logout', require('./controllers/auth/logout.controller'))

app.use('/users', authMiddleware.auth, require('./routes/user.route'))
app.use('/products', require('./routes/product.route'))
app.use('/cart', require('./routes/cart.route'))

app.use(require('./controllers/error/errorController').notFoundController)

app.listen(PORT, () => {
    console.log(`port running on http://localhost:${PORT}`)
})
